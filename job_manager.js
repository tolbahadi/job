'use strict';

var helpers = require('helpers'),
    BackboneEvents = require('backbone-events-standalone');


function JobManager (options) {
    this.currentJobs = {};
    if (options && options.jobType) {
        this.jobType = options.jobType;
    }
}

function validateKey (self, key) {
    if (!key) {
        throw new Error('invalid key argument');
    }

    if (self.currentJobs[key]) {
        throw new Error('a job with the same key already exists');
    }
}

JobManager.prototype = {
    _forwardJobEvent: function (eventName, job, results) {
        this.trigger(eventName, this, job, results);
    },

    // takes a JobProcess or JobScheduler instance with a unique key string;
    // creates a new JobScheduler if necessary and adds it to the currentJobs has
    addJob: function (key, job) {
        validateKey(this, key);

        this.listenTo(job, 'all', this._forwardJobEvent);
        this.listenTo(job, 'destroy', function () {
            this._removeJob(job, key);
        });
        this.currentJobs[key] = job;
        this.trigger('jobAdded', this, job);

        return job;
    },
    removeJob: function (key) {
        var job = this.getJob(key);

        if (!job) {
            throw 'Job not found!';
        }

        job.destroy();
    },
    _removeJob: function (job, key) {
        this.stopListening(job);
        delete this.currentJobs[key];

        this.trigger('jobRemoved', this, job);

        return job;
    },
    createJob: function (key, options) {
        validateKey(this, key);

        var job = new this.jobType(options);

        this.trigger('jobCreated', this, job);

        this.addJob(key, job);

        return job;
    },
    getCurrentJobs: function () {
        return this.currentJobs;
    },
    getJob: function (key) {
        return this.currentJobs[key];
    }
};

BackboneEvents.mixin(JobManager.prototype);

JobManager.extend = helpers.extend;

module.exports = JobManager;