'use strict';

require('should');

var JobManager = require('../job_manager'),
    JobScheduler = require('../job_scheduler');

describe('JobManager module', function () {
    describe('#constructor', function () {
        it('JobManager is defined', function () {
            JobManager.should.be.ok;
        });
        it('Initialize new job manager', function () {
            var jm = new JobManager();
            jm.should.be.ok;
            jm.should.be.an.instanceOf(JobManager);
        });
    });
    describe('#createJob', function () {
        beforeEach(function () {
            this.jm = new JobManager({
                jobType: JobScheduler
            });
        });
        // jshint immed:false
        it('passing invalid key', function () {
            var self = this;
            (function () {
                self.jm.createJob(null, {});
            }).should.throw (/^invalid key argument/);

            (function () {
                self.jm.createJob('', {});
            }).should.throw(/^invalid key argument/);
        });
        it('passing existing key', function () {
            var self = this;
            self.jm.createJob('job1', {});
            (function () {
                self.jm.createJob('job1', {});
            }).should.throw('a job with the same key already exists');
        });
        it('passing a valid object', function () {
            var js = this.jm.createJob('job1', {name: 'job_1'});
            js.should.be.ok;
            js.should.be.an.instanceOf(JobScheduler);
        });
    });
    describe('#getCurrentJobs', function () {
        beforeEach(function () {
            this.jm = new JobManager({jobType: JobScheduler});
        });
        it('getCurrentJobs should return empty an object', function () {
            this.jm.getCurrentJobs().should.be.ok;
            Object.keys(this.jm.getCurrentJobs()).should.be.empty;
        });
        it('adding one job', function () {
            var js = this.jm.createJob('job1', {key: 'job1'});
            this.jm.getCurrentJobs().should.be.ok;
            this.jm.getCurrentJobs().job1.should.eql(js);
            this.jm.getCurrentJobs().should.eql({
                job1: js
            });
        });
        it('adding two jobs', function () {
            var js1 = this.jm.createJob('job1', {key: 'job1'});
            var js2 = this.jm.createJob('job2', {key: 'job2'});
            this.jm.getCurrentJobs().should.be.eql({job1: js1, job2: js2});
        });
    });
    describe('#getJob', function () {
        before(function () {
            this.jm = new JobManager({jobType: JobScheduler});
        });
        it('add job1', function () {
            var js1 = this.jm.createJob('job1');
            var js2 = this.jm.createJob('job2');

            this.jm.getJob('job1').should.eql(js1);
            this.jm.getJob('job2').should.eql(js2);
        });
    });
});