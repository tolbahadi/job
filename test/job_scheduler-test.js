'use strict';

require('should');

var JobScheduler = require('../job_scheduler'),
    sinon = require('sinon');

describe('JobScheduler module', function () {
    describe('JobScheduler#contructor', function () {
        var job;
        before(function () {
            job = new JobScheduler();
        });
        it('JobScheduler#initialized', function () {
            job.should.be.ok;
        });
        it('JobScheduler#schedule', function () {
            job.schedule.should.be.ok;
        });
        it('JobScheduler#cancel', function () {
            job.cancel.should.be.ok;
        });
        it('JobScheduler#invoke', function () {
            job.invoke.should.be.ok;
        });
    });
    describe('JobScheduler#invoke', function () {
        it('JobScheduler#run', function () {
            var job = new JobScheduler();
            sinon.spy(job, 'run');
            job.invoke();
            job.run.called.should.be.ok;
        });
    });

    describe('JobScheduler#schedule', function () {
        it('JobScheduler#schedule duration', function (done) {
            var startTime,
                duration = 10, job;

            job = new (JobScheduler.extend({
                run: function () {
                    var endTime = +new Date();
                    (endTime - startTime).should.be.approximately(duration, 20);
                    done();
                }
            }))();

            startTime = +new Date();

            job.schedule(new Date(startTime + duration));
        });
    });

    describe('JobScheduler#isScheduled,getNextScheduleDate,cancel', function () {
        var duration = 10;

        beforeEach(function () {
            this.job = new JobScheduler(this.jobProcess);
        });

        it('isScheduled', function () {
            this.job.schedule(new Date() + duration);
            this.job.isScheduled().should.be.true;
        });

        it('getNextScheduleDate', function () {
            var expectedScheduleDate = new Date(new Date().getTime() + duration);
            this.job.schedule(expectedScheduleDate);
            this.job.getNextScheduleDate().should.eql(expectedScheduleDate);
        });

        it('cancel', function () {
            this.job.run = function () { throw 'run should not get called'; };
            var expectedScheduleDate = new Date(new Date().getTime() + 5);
            this.job.schedule(expectedScheduleDate);
            this.job.cancel();
            this.job.isScheduled().should.not.be.true;
            (this.job.getNextScheduleDate() == null).should.be.ok;
        });
    });
});