'use strict';

var Q = require('q'),
    schedule = require('node-schedule'),
    helpers = require('helpers'),
    _ = require('underscore'),
    BackboneEvents = require('backbone-events-standalone');

function JobScheduler (options) {
    this.options = options;
    // start new scheduler for jobProcess
    this._scheduleJob = new schedule.Job(_.bind(this.invoke, this));
}

JobScheduler.prototype = {
    run: function () {

    },
    invoke: function () {
        // run job process
        // jshint newcap:false
        this.trigger('before-run', this);
        var runPromise = Q(this.run()),
            self = this;

        self.trigger('run', this, runPromise);

        runPromise.done(function (result) {
            self.trigger('after-run', this, result);
        });

        return runPromise;
    },
    schedule: function (date) {
        // if date is a number, convert it into a date object
        if (_.isNumber(date) || _.isString(date)) {
            date = new Date(date);
        }
        // schedule job process
        return this._scheduleJob.schedule(date);
    },
    isScheduled: function () {
        return this._scheduleJob.nextInvocation() != null;
    },
    getNextScheduleDate: function () {
        return this._scheduleJob.nextInvocation();
    },
    cancel: function () {
        // cancel job process
        this._scheduleJob.cancel();
    },
    destroy: function () {
        if (this.isScheduled()) {
            this.cancel();
        }
        this.trigger('destroy', this);
        // clean up
    }
};

BackboneEvents.mixin(JobScheduler.prototype);

JobScheduler.extend = helpers.extend;

exports = module.exports = JobScheduler;
